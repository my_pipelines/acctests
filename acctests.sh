#!/bin/bash

sleep 5

curl -sf localhost:9999/getresult | jq . > result-curl.json
diff result-curl.json result.json
ret=$(echo $?)

if [ $ret -ne 0 ]; then
  echo "falha no teste getresult"
  exit 1
fi

curl -sf localhost:9999/vote?movie=1984 | jq . > vote-curl.json
diff vote-curl.json vote.json
ret=$(echo $?)

if [ $ret -ne 0 ]; then
  echo "falha no teste vote"
  exit 1
fi

curl -sf localhost:9999/getresult | jq . > computedvote-curl.json
diff computedvote-curl.json aftervote.json
ret=$(echo $?)

if [ $ret -ne 0 ]; then
  echo "falha no teste Aftervote"
  exit 1
fi
